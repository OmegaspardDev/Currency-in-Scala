package concrete

import `abstract`.CurrencyZone

object Europe extends CurrencyZone{
  abstract class Euro extends AbstractCurrency {
    def designation = "EUR"
  }

  override type Currency = Euro

  override def make(cents: Long) = new Euro {
    val amount = cents
  }

  val Cent = make(1)
  val Euro = make(100)
  override val CurrencyUnit = Euro
}
