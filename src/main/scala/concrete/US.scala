package concrete
import `abstract`.CurrencyZone

object US  extends CurrencyZone{
  abstract class Dollar extends AbstractCurrency {
      def designation = "USD"
  }

  override type Currency = Dollar

  override def make(cents: Long) = new Dollar {
    val amount = cents
  }

  val Cent = make(1)
  val Dollar = make(100)
  override val CurrencyUnit = Dollar
}
